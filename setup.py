from setuptools import setup, find_packages  

setup(
    name = 'strandtools',
    packages = find_packages(),
    author='Marco R. Cosenza',
    author_email='marco.cosenza@embl.de',
    version='0.0.1'
    )
