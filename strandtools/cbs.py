# CIRCULAR BINARY SEGMENTATION
import numpy as np
import pandas as pd
import multiprocessing as mp
from time import time

def tstat(x, i):
    n = len(x)
    s0 = np.mean(x[:i])
    s1 = np.mean(x[i:])
    return (n-i)*i/n*(s0-s1)**2


def test_significance(x, cp, t, n_shuffles=1000, p=0.05):
    '''Test t statistic associated with the segment changepoint for statistical significance. 
    This is done by calculating the t statistic distribution from n shuffles of the segment and calculates p-value.'''
    xs = x.copy()
    # x: data, cp: changepoint, t: threshold (tstat)
    alpha = n_shuffles*p
    t_count = 0
    for i in np.arange(n_shuffles):
        np.random.shuffle(xs)
        t_count += (tstat(xs, cp) >= t)
    return t_count <= alpha


def find_edge(args):
    '''Use t statistic to detect the edge (changepoint) in a segment.
    Every bin is considered as candidate changepoint and these are ordered according to the resulting t statistic between
    the two derived segment splits.
    The first n (depth) candidate changepoints are then tested for significance. If significant, the bin coordinate (plus 1) is returned.
    Otherwise, the starting coordinate is returned.'''

    segm, edge, start, depth, n_shuffles, p = args
    # if 0s are present then the segment was already
    # completely tested
    if (len(segm) <= 0) or (edge == 0).any():
        return start
    # step 1 order candidate changepoints by difference of means
    tstats = [] 
    found = False
    for i in np.arange(segm.shape[0]):
        tstats.append(tstat(segm, i)) # t statistic

    # order statistic by size
    tstats = np.array(tstats)
    tstats[np.isnan(tstats)] = 0
    order = np.argsort(np.power(tstats,2))[::-1]
    order = order[np.logical_not(np.isin(order, [0,1,segm.shape[0]-1,segm.shape[0]-2]))]

    # step 2 test changepoints for significance
    for j in order[:depth]:
        if test_significance(segm, j, tstats[j], n_shuffles=n_shuffles, p=p):
            edge[j+1] = 1
            found = True
            break
    if found:
        return start + j+1

    # if all changepoints are tested and no significant one was found
    # this segment does not need to be tested further
    edge[np.where(edge==-1)[0]] = 0
    return start


def circular_binary_segmentation_iteration_multiprocessing(data, edges, pool, depth=1, n_shuffles=1000, p=0.05):
    '''Given edge positions, split data into segments and find edges within each segment (if present).
    Uses multiprocessing to parallelize calculations over segments.'''
    starts = np.where(edges == 1)[0]
    depth = np.repeat(n_shuffles, starts.shape[0]+1)
    n_shuffles = np.repeat(n_shuffles, starts.shape[0]+1)
    p = np.repeat(p, starts.shape[0]+1)
    
    segments_split = np.split(data, starts)
    edges_split = np.split(edges, starts)
    
    args = list(zip(segments_split, edges_split, np.concatenate([[0], starts]), depth, n_shuffles, p))
    out = pool.map(find_edge, args)
    
    out = np.array(out)
    done_idxs = np.where(np.isin(starts,out))[0]

    for idx in done_idxs:
        edges_split[idx][edges_split[idx]==-1] = 0
    segm_out = np.concatenate(edges_split)
    segm_out[out] = 1

    return segm_out


def circular_binary_segmentation_multiprocessing(data, edge_init, n_workers=-1, pool=None, depth=1, n_shuffles=1000, p=0.05, tol=0, max_iter=100):
    '''Circular binary segmentation implementation with multiprocessing.
    Segmentation stops when maximum number of iteration is reached or when no new edges are added.'''
    if pool is None:
        n_workers = mp.cpu_count() if n_workers == -1 else n_workers
        pool = mp.Pool(n_workers)

    n_edges = (edge_init == 1).sum()
    edges = edge_init.copy()
    
    for n in np.arange(max_iter):
        t0 = time()
        edges = circular_binary_segmentation_iteration_multiprocessing(data, edges, pool=pool, depth=depth, n_shuffles=n_shuffles, p=p)
        edges[edge_init == 1] = 1
        t1 = time()
        # loop breakers
        print('iteration {}\t\tnum edges {}\t\ttime {}'.format(n,n_edges,t1-t0))
        if (n_edges - (edges==1).sum()) == tol:
            print('tolerance reached')
            break
        else:
            n_edges = (edges==1).sum()
        if n >= max_iter:
            print('maximum iteration reached')
            break
            
    return edges 


    # LEGACY FUNCTIONS
    # without multiprocessing

#     def circular_binary_segmentation_iteration(segments, edges, n_shuffles=1000, p=0.05):
#     segments_split = np.split(segments, np.where(edges == 1)[0])
#     edges_split = np.split(edges, np.where(edges == 1)[0])
    
#     for segm, edge in zip(segments_split, edges_split):
#         # if 0s are present then the segment was already
#         # completely tested
#         if len(segm) <= 0:
#             continue
#         if (edge == 0).any():
#             continue
        
#         # step 1 order candidate changepoints by difference of means
#         tstats = [] 
#         found = False
#         for i in np.arange(segm.shape[0]):
#             tstats.append(tstat(segm, i)) # t statistic
        
#         # order statistic by size
#         tstats = np.array(tstats)
#         tstats[np.isnan(tstats)] = 0
#         order = np.argsort(np.power(tstats,2))[::-1]
#         order = order[np.logical_not(np.isin(order, [0,1,segm.shape[0]-1,segm.shape[0]-2]))]

#         # step 2 test changepoints for significance
#         for j in order:
#             if test_significance(segm, j, tstats[j], n_shuffles=n_shuffles, p=p):
#                 edge[j+1] = 1
#                 found = True
#                 break
#         if found:
#             continue
        
#         # if all changepoints are tested and no significant one was found
#         # this segment does not need to be tested further
#         edge[np.where(edge==-1)[0]] = 0
    
#     edges = np.concatenate(edges_split)
#     return edges


# def circular_binary_segmentation(data, edge_init, n_shuffles=1000, p=0.001, tol=0, max_iter=100):
#     n_edges = (edge_init == 1).sum()
#     edges = edge_init.copy()
#     for n in np.arange(max_iter):
#         edges = circular_binary_segmentation_iteration(data, edges, n_shuffles=n_shuffles, p=p)

#         # loop breakers
#         print('num edges', n_edges)
#         if (n_edges - (edges==1).sum()) == tol:
#             print('tolerance met')
#             break
#         else:
#             n_edges = (edges==1).sum()
#         if n >= max_iter:
#             print('max iter met')
#             break
            
#     return edges 