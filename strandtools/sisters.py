import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def match_sisters(df, plot=True):
    '''detect sister cells by looking at strand state anti-correlation'''
    mat = pd.pivot_table(data=df, index=['chrom', 'start', 'end'], columns='cell', values='class', aggfunc='first')
    mat.replace('WW',  1, inplace=True)
    mat.replace('WC',  0, inplace=True)
    mat.replace('CC', -1, inplace=True)

    corr = mat.iloc[:,:-1].corr()

    if plot:
        fig, ax = plt.subplots(figsize=(12,12))
        sns.heatmap(corr, 
            vmax=-0.5, vmin=-1, cmap='Spectral',
            square=True, 
            xticklabels=True, ax=ax)
    
    sisters = corr.apply(lambda row: row.index[np.argmin(row)] if np.min(row) < -.9 else np.nan)
    sisters = sisters[~sisters.isna()]
    sisters = sisters.reset_index()
    sisters.columns = ['c1', 'c2']
    
    # remove duplicates
    c1 = []
    c2 = []
    for q,m in zip(sisters['c1'], sisters['c2']):
        if (m not in c1) and (m not in c2):
            c1.append(q)
            c2.append(m)

    sisters = pd.DataFrame({'c1':c1, 'c2':c2})
    return sisters