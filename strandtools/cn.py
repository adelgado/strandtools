import os
import numpy as np
import pandas as pd
import warnings
from tqdm import tqdm
import multiprocessing as mp

# strandtools
from .normalization import merge_bins
from .plot import reorder_chroms
from .cbs import circular_binary_segmentation_multiprocessing
from .filter import qc_pass, get_changepoints

# visualization
import matplotlib.pyplot as plt
import seaborn as sns
import colorcet as cc

# sklearn
from sklearn.cluster import OPTICS
from sklearn.semi_supervised import LabelPropagation
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import NearestNeighbors
from sklearn.calibration import CalibratedClassifierCV
from sklearn.metrics import calinski_harabasz_score

# scipy
from scipy.stats import zscore
from scipy.optimize import brute, fmin
from scipy.signal import find_peaks, peak_widths


class CopyNumberClassifier():
    def __init__(self):
        
        # data
        self.counts = None
        self.counts_merged = None
        self.counts_filtered = None

        # training switches
        self.GNB_wc_status = 0
        self.GNB_ww_status = 0
        self.cn_assignment_status = 0

        # progress switches
        self._supersegments = False
        self._changepoints = False
        self._pred_qc = False


    def load_counts(self, counts):
        if isinstance(counts, str) and os.path.exists(counts):
            counts = pd.read_csv(counts, index_col=0)

        # formatting
        if counts['w'].isna().any():
            warnings.warn('w column contains {} bins with NA values. Filling NAs with 0'.format(counts['w'].isna().sum()))
            counts['w'].fillna(0, inplace=True)
        if counts['c'].isna().any():
            warnings.warn('c column contains {} NA values. Filling NAs with 0'.format(counts['c'].isna().sum()))
            counts['c'].fillna(0, inplace=True)
        counts['class'].fillna('None', inplace=True)
        if not counts.columns.isin(['tot_count']).any():
            counts['tot_count'] = counts['w'] + counts['c']
        counts['chrom'] = pd.Categorical(counts['chrom'], categories=reorder_chroms(counts['chrom'].unique()).values)
        counts.sort_values(by=['sample', 'cell', 'chrom', 'start'], inplace=True)
        counts['chrom'] = counts['chrom'].astype(str)
        self.counts = counts


    def merge_bins(self, bin_size=2e6):
        counts_merged = merge_bins(self.counts.copy(), bin_size=bin_size, agg_mode='mean')
        counts_merged['wf'] = counts_merged['w'] / counts_merged['tot_count']
        self.counts_merged = counts_merged


    def cbs_segmentation(self, exclusion_bed=None, low_threshold=5, depth=1, max_iter=30, tol=0, n_shuffles=100, p=0.001, n_workers=-1):
        counts = self.counts.copy()

        # edge initialization
        counts.loc[:,'edge_init'] = -1
        counts.loc[:,'excluded'] = False
        counts.loc[counts.groupby(['cell', 'chrom']).head(1).index, 'edge_init'] = 1
        # better WC class definition
        w_smoothened = counts['w'].rolling(window=5, step=1, center=True, min_periods=3).median()
        tot_counts_smoothened = counts['tot_count'].rolling(window=5, step=1, center=True, min_periods=3).median()
        wf = w_smoothened / tot_counts_smoothened
        counts['class2'] = np.nan
        counts.loc[wf < .1, 'class2'] = 'CC'
        counts.loc[wf >= .9, 'class2'] = 'WW'
        counts.loc[(wf >= .1)&(wf < .9), 'class2'] = 'WC'
        counts['class2'].fillna(method='backfill', inplace=True)
        counts.loc[counts['class']=='None', 'class2'] = 'None' # transfer None labels
        counts.loc[counts['class2'] != counts['class2'].shift(1), 'edge_init'] = 1
        #counts.loc[counts['class'] != counts['class'].shift(1), 'edge_init'] = 1
        if exclusion_bed is not None:
            exclusion_bed = pd.read_csv(exclusion_bed, sep='\t', header=None)
            exclusion_bed.rename({0: 'chrom', 1: 'start', 2: 'end'}, axis='columns', inplace=True)
            for n, row in tqdm(exclusion_bed.iterrows()):
                for ce, g in counts.groupby('cell'):
                    sel = (g['chrom']==row['chrom'])&(g['start']>=row['start'])&(g['end']<=row['end'])
                    if sel.sum() == 0:
                        continue
                    sel_idxs = g.index[sel]
                    counts.loc[sel_idxs,'edge_init'] = 0
                    counts.loc[sel_idxs,'excluded'] = True
                    counts.loc[sel_idxs[0],'edge_init'] = 1
                    counts.loc[sel_idxs[-1],'edge_init'] = 1

        edges_init = counts['edge_init'].values
        w = counts['w'].values
        c = counts['c'].values

        n_workers = mp.cpu_count() if n_workers == -1 else n_workers
        P = mp.Pool(n_workers)

        wedges = circular_binary_segmentation_multiprocessing(w, edges_init, pool=P, max_iter=max_iter, tol=tol, depth=depth, n_shuffles=n_shuffles, p=p)
        cedges = circular_binary_segmentation_multiprocessing(c, edges_init, pool=P, max_iter=max_iter, tol=tol, depth=depth, n_shuffles=n_shuffles, p=p)

        cbs_edges = ((wedges==1) + (cedges==1)) >= 1
        counts.loc[:, 'cbs_edges'] = cbs_edges
        counts.loc[:, 'segm_id_1'] = counts['cbs_edges'].cumsum()

        self.counts.loc[:, 'edge_init'] = counts['edge_init']
        self.counts.loc[:, 'excluded'] = counts['excluded']
        self.counts.loc[:, 'cbs_edges'] = cbs_edges
        self.counts.loc[:, 'segm_id_1'] = self.counts['cbs_edges'].cumsum()

        counts_merged = merge_bins(counts, bin_size=None, bin_idxs=counts['segm_id_1'], agg_mode='stats')
        counts_merged.rename({'merged_bin': 'segm_id_1', 'tot_count_count': 'n_bins'}, axis='columns', inplace=True)
        counts_merged['wf'] = counts_merged['w'] / counts_merged['tot_count']
        self.counts_merged = counts_merged
    

    def filter_counts(self, wc_threshold=None, low_threshold=None, size_threshold=None, nn_distance=None, plot=True):
        counts = self.counts_merged

        filter_cols = ['tot_count_ref',
       'log_ratio', 'wf_pass', 'bin_mean', 'low_pass', 'z_tot_count', 'z_wf',
       'z_log_ratio', 'size', 'size_pass', 'QC_pass']
        if counts.columns.isin(filter_cols).any():
            counts = counts.loc[:, ~counts.columns.isin(filter_cols)]
            warnings.warn('discarding previously existing filter columns')

        # deviation from bin average
        ref = counts.groupby(['chrom','start']).agg({'tot_count': 'mean'}).reset_index()
        #counts = counts.loc[:, counts.columns.isin([
        #    'cell', 'chrom', 'start', 'end', 'class', 'w', 'c', 'tot_count', 'tot_count_std', 'n_bins', 'wf', 'segm_id_1', 'super_segm_id'])] # reset previous calculations, if any
        counts = pd.merge(counts, ref, on=['chrom','start'], how='left', suffixes=['', '_ref'])
        counts['log_ratio'] = np.log(counts['tot_count']+1e-9) - np.log(counts['tot_count_ref']+1e-9)
        
        # FILTERING
        # filter WC only
        if wc_threshold is None:
            wc_threshold = (0.1, 0.9)
        else:
            wc_threshold = (wc_threshold, 1-wc_threshold)
        counts['wf_pass'] = counts['wf'].between(*wc_threshold) & counts['class'].isin(['WC'])
        
        # filter low count bins
        # estimate low threshold
        bin_stats = counts[counts['tot_count']>0].groupby(['chrom', 'start'])['tot_count'].describe()
        bin_stats.rename({'mean': 'bin_mean'}, axis='columns', inplace=True)
        counts = pd.merge(counts, bin_stats.loc[:,'bin_mean'].reset_index(), how='left', on=['chrom', 'start'])
        if (low_threshold is None):
            low_threshold = (bin_stats['50%'].mean() - (bin_stats['50%'].std() *3))
        #counts['low_pass'] = counts['bin_mean'] > low_threshold
        counts['low_pass'] = counts['tot_count'] > low_threshold

        if plot:
            fig, ax = plt.subplots(figsize=(9,3), ncols=2, width_ratios=(2,1))
            sns.histplot(bin_stats['50%'], bins=128, ax=ax[0])
            ax[0].axvline(low_threshold, linestyle='--', color='gray', alpha=.7)
            sns.scatterplot(data=counts, x='tot_count', y='wf',
                            marker='.', linewidth=0, alpha=.33,
                            ax=ax[1])
            ax[1].axvline(low_threshold, linestyle='--', color='gray', alpha=.7)
            ax[1].axhline(wc_threshold[0], linestyle='--', color='gray', alpha=.7)
            ax[1].axhline(wc_threshold[1], linestyle='--', color='gray', alpha=.7)
            ax[1].set_title('low-count and WC thresholds')
        
        counts.loc[counts['tot_count']>0, ['z_tot_count', 'z_wf', 'z_log_ratio']] = zscore(counts.loc[counts['tot_count']>0, ['tot_count', 'wf', 'log_ratio']], axis=0).values
        
        if (size_threshold is not None):
            counts['size'] = (counts['end'] - counts['start']) / 1e6
            counts['size_pass'] = counts['size'] > size_threshold
            if plot:
                fig, ax = plt.subplots(figsize=(3,3))
                sns.scatterplot(data=counts, 
                                x='tot_count', y='wf', color='gray', alpha=1,
                                marker='.', linewidth=0, ax=ax)
                sns.scatterplot(data=counts.loc[counts['size_pass'],:].sort_values(by='size'), 
                                x='tot_count', y='wf', hue='size', palette=cc.cm.rainbow, alpha=1,
                                marker='.', linewidth=0, ax=ax)
                ax.axvline(low_threshold, linestyle='--', color='black', alpha=.7)
                ax.axhline(wc_threshold[0], linestyle='--', color='black', alpha=.7)
                ax.axhline(wc_threshold[1], linestyle='--', color='black', alpha=.7)
                ax.set_title('size thresholding')

        qc_cols = counts.columns.isin(['low_pass', 'wf_pass', 'size_pass'])
        if qc_cols.sum() > 0:
            counts['QC_pass'] = counts.loc[:,qc_cols].all(axis=1)
        else:
            counts['QC_pass'] = True
        self.counts_merged = counts.copy()

        counts_filt = counts.loc[counts['QC_pass'], :]
        self.counts_filtered = counts_filt.copy()

        if nn_distance is not None:
            X = counts_filt.loc[:,['z_tot_count', 'z_wf', 'z_log_ratio']]
            nbrs = NearestNeighbors(n_neighbors=20, radius=.6).fit(X)
            distances, _ = nbrs.kneighbors(X)
            counts_filt.loc[:,'n20_distance'] = distances.max(axis=1)
            counts
            counts_filt = counts_filt.loc[counts_filt['n20_distance']<=nn_distance, :]
            self.counts_filtered = counts_filt
        
            if plot:
                fig, ax = plt.subplots(figsize=(6,3), ncols=2)
                sns.scatterplot(data=counts_filt[counts_filt['n20_distance']<=nn_distance].sort_values(by='n20_distance'), 
                                x='tot_count', y='wf', hue='n20_distance', palette=cc.cm.rainbow, alpha=1,
                                marker='.', linewidth=0, ax=ax[1])
                sns.move_legend(ax[1], 'upper left', bbox_to_anchor=(1,1))
                sns.histplot(counts_filt['n20_distance'], ax=ax[0])
                ax[0].axvline(nn_distance, linestyle='--', color='lightgray')

        if plot:
            fig, ax = plt.subplots(figsize=(3,3))
            sns.scatterplot(data=self.counts_merged, 
                            x='tot_count', y='wf', color='gray', alpha=1,
                            marker='.', linewidth=0, ax=ax)
            sns.scatterplot(data=self.counts_filtered, 
                            x='tot_count', y='wf', color='red', alpha=1,
                            marker='.', linewidth=0, ax=ax)
            ax.axvline(low_threshold, linestyle='--', color='black', alpha=.7)
            ax.axhline(wc_threshold[0], linestyle='--', color='black', alpha=.7)
            ax.axhline(wc_threshold[1], linestyle='--', color='black', alpha=.7)
            ax.set_title('filtered data-points')
   

    def compute_reachability(self, n_subsample=None, min_samples=7, max_eps=1, random_state=None, plot=False):
        n_max = self.counts_filtered.shape[0]
        if n_subsample is None:
            n_subsample = 1000 if n_max >= 1000 else n_max
        elif n_subsample > n_max:
            n_subsample = n_max
            warnings.warn('n_subsample clipped at {}'.format(n_max))
        counts = self.counts_filtered.sample(n=n_subsample, random_state=random_state)

        self.opt = OPTICS(min_samples=min_samples, max_eps=max_eps, metric='euclidean')
        # we rescale to make the distance in tot_count and wf comparable        
        self.norm_coeffs = {'tot_count': counts['tot_count'].max(), 'wf': counts['wf'].max()}
        self.training_WC = counts.copy()
        self.training_WC['tot_count'] = self.training_WC['tot_count'] / self.norm_coeffs['tot_count']
        #self.training_WC['wf'] = self.training_WC['wf'] / self.norm_coeffs['wf']
        
        self.opt.fit(self.training_WC.loc[:,['tot_count', 'wf']].values)
        self.training_WC['reachability'] = self.opt.reachability_
        fill = self.training_WC['reachability'][self.training_WC['reachability'] != np.inf].max()
        self.training_WC['reachability'].replace({np.inf: fill}, inplace=True)

        if plot:
            yy = self.opt.reachability_[self.opt.ordering_]
            xx = np.arange(len(yy))
            fig, ax = plt.subplots(figsize=(9,3), ncols=2, width_ratios=(2,1))
            sns.scatterplot(x=xx, y=yy, 
                            marker='.', linewidth=0, ax=ax[0])
            ax[0].set_ylabel('reachability')
            ax[0].set_xlabel('ordered data-points')
            sns.scatterplot(data=self.training_WC.sort_values(by='reachability'), x='tot_count', y='wf', 
                            hue='reachability', palette='viridis', 
                            marker='.', linewidth=0, ax=ax[1])
            sns.move_legend(ax[1], 'upper left', bbox_to_anchor=(1,1))


    def estimate_prominence(self, prominence_min=0, prominence_max=0.04, prominence_steps=100):
        re = self.opt.reachability_[self.opt.ordering_]
        to_test = np.linspace(prominence_min, prominence_max, prominence_steps)
        scores = []
        for p in tqdm(to_test):
            # propagate labels because otherwise the -1 cluster is considered another 'proper' cluster
            self.extract_clusters(prominence=p, propagate_labels=True)
            if self.training_WC['cluster'].unique().shape[0] == 1:
                scores.append((p, np.nan))
            else:
                score = calinski_harabasz_score(self.training_WC[['z_tot_count', 'z_wf']], self.training_WC['cluster'])
                scores.append((p, score))
        scores = np.array(scores)
        best_idx = np.nanargmax(scores[:,1])
        best_prominence = scores[best_idx, 0]
        return best_prominence


    def extract_clusters(self, prominence=None, edges=None, ranges=None, propagate_labels=False, plot=False):
        
        counts = self.training_WC

        yy = self.opt.reachability_[self.opt.ordering_]
        xx = np.arange(len(yy))

        if prominence is None:
            prominence = self.estimate_prominence()

        pp, ppf = find_peaks(yy, prominence=prominence)

        if plot:
            ppw = peak_widths(yy, pp, rel_height=1)

            fig, ax = plt.subplots(figsize=(9,6), nrows=2, ncols=2, height_ratios=(1,1), width_ratios=(2,1))
            sns.scatterplot(x=xx, y=yy, 
                            marker='.', linewidth=0, ax=ax[0,0])
            ax[0,0].set_ylabel('reachability')
            ax[0,0].set_xlabel('ordered data-points')
            sns.scatterplot(x=pp, y=yy[pp], color='tomato', marker='.', s=200, ax=ax[0,0])
            
            top, _ = ax[0,0].get_ylim()
            for n,p in enumerate(pp):
                # increase n by 1 because peaks will also include start and end of data
                # to define valid edges
                ax[0,0].annotate('P'+str(n+1).zfill(len(str(len(pp)))), (p, yy[p]+(top*0.5)) ) 
            ax[0,0].hlines(*ppw[1:], color="C3")
            ax[0,0].vlines(x=pp, ymin=(yy[pp])-ppf['prominences'], ymax=yy[pp], color='red')
            
            ax[0,1].set_axis_off()

        # compute edges
        if edges is None:
            edges = list(zip(np.concatenate([[0], pp]), np.concatenate([pp, [len(xx)-1]])))
        else:
            all_peaks = np.concatenate([[0], pp, [len(xx)-1]])
            for n, (l,r) in enumerate(edges):
                edges[n] = (all_peaks[l], all_peaks[r])
        
        # ranges overrides edges
        if ranges is not None:
            edges = ranges   

        # extract cluster labels
        cl =np.zeros_like(xx)
        cl[:] = -1
        for n, (l,r) in enumerate(edges):
            minimum = np.min(yy[[l,r]])
            sel1 = np.where((xx>=l)*(xx<=r), True, False)
            sel2 = yy < minimum
            cl[sel1*sel2] = n
        self.clusters = np.zeros_like(cl)
        self.clusters[self.opt.ordering_] = cl

        # extract reachability
        weights = np.zeros_like(cl, dtype=np.float64)
        r = self.opt.reachability_.copy()
        r[np.isinf(r) == True] = 0
        weights[self.opt.ordering_] = r
        weights = weights/np.nanmax(weights)
        self.weights = weights
        
        # label propagation
        if propagate_labels:
            lbl_prop = LabelPropagation(n_jobs=-1, kernel='knn', n_neighbors=100)
            lbl_prop.fit(counts.loc[:,['z_tot_count', 'z_wf', 'z_log_ratio']], self.clusters)
            self.clusters = lbl_prop.transduction_

        counts.loc[:, 'cluster'] = self.clusters
        counts.loc[:, 'weight'] = weights

        self.training_WC = counts

        if plot:

            for col, (l,r) in zip(cc.glasbey_dark[1:len(edges)+1], edges): # take into account -1 cluster
                ax[1,0].axvspan(l,r, facecolor=col, linestyle='--', color=col, alpha=.3)

            sns.scatterplot(x=xx, y=yy, 
                            legend=False,
                            marker='.', linewidth=0, ax=ax[1,0])
            ax[1,0].locator_params(axis='x', tight=True, nbins=10)
            ax[1,0].set_ylabel('reachability')
            ax[1,0].set_xlabel('ordered data-points')

            sns.scatterplot(data=counts, x='z_tot_count', y='z_wf',
                            hue=self.clusters, palette=cc.glasbey_dark, alpha=1,
                            legend='full',
                            marker='.', linewidth=0,
                            ax=ax[1,1])
            sns.move_legend(ax[1,1], 'upper left', bbox_to_anchor=(1,1))

            fig.tight_layout()
    
        return pp, ppf


    def fit_WC(self, counts=None, calibrated=False, plot=False):
        
        counts = self.training_WC.copy()
            
        if self.GNB_wc_status == 0:
            self.GNB_wc = GaussianNB()
            self.GNB_wc.fit(X=counts.loc[:,['tot_count', 'wf']].fillna(0).values,
                            y=counts.loc[:,'cluster'].values,
                            sample_weight=counts.loc[:,'weight'].values)
            if calibrated:
                cal_GNB_wc = CalibratedClassifierCV(self.GNB_wc, cv=3, method='sigmoid', n_jobs=-1)
                cal_GNB_wc.fit(X=counts.loc[:,['tot_count', 'wf']].fillna(0).values,
                               y=counts.loc[:,'cluster'].values,
                               sample_weight=counts.loc[:,'weight'].values)
                self.GNB_wc = cal_GNB_wc


        if plot:
            X = counts.loc[:,['tot_count', 'wf']].fillna(0).values # predict on all datapoints
            fig, ax = plt.subplots(figsize=(6,3), ncols=2)
            lbls = self.GNB_wc.predict(X)
            if -1 in lbls:
                prob = np.max(self.GNB_wc.predict_proba(X)[:,1:], axis=1)
            else:
                prob = np.max(self.GNB_wc.predict_proba(X), axis=1)

            sns.scatterplot(x=X[:,0], y=X[:,1],
                            hue=prob, legend=False, palette=cc.cm.rainbow,
                            linewidth=0, marker='.', alpha=.7,
                            ax=ax[0])
            sns.scatterplot(x=X[:,0], y=X[:,1],
                            hue=lbls, legend='full', palette=cc.glasbey_dark,
                            linewidth=0, marker='.', alpha=.5,
                            ax=ax[1])
            sns.move_legend(ax[1], 'upper left', bbox_to_anchor=(1,1))

        self.GNB_wc_status = 1


    def fit_copy_number(self, cnmin=2, cnmax=5, plot=False):
        if self.cn_assignment_status == 0:
            # assign copy number states
            cnmin_wc = np.max([cnmin, 2]) # lower bound to 2
            
            if isinstance(self.GNB_wc, CalibratedClassifierCV):
                th = self.GNB_wc.estimator.theta_
            else:
                th = self.GNB_wc.theta_
            #if there's only one cluster, assignment is solved already
            if len(th[1:,1]) == 1:
                self.cn_wc = [-1, cnmin_wc]
            else:
                er, cns = self.get_ratios(nmin=0, nmax=1, cnmin=cnmin_wc, cnmax=cnmax)
                cns = np.array(cns)
                
                self.ranges = []
                start = 1 if (-1 in self.clusters) else 0
                for ra in th[start:,1]: # exclude label -1
                    ra = np.unique(er[:,1])[np.argmin((ra - np.unique(er[:,1]))**2)] # clip to closest ratio
                    self.ranges.append(cns[np.where(er[:,1] == ra)])
                self.constraints = [slice(0,len(rng),1) for rng in self.ranges]
                
                # solve assignment by brute force
                idxs, corr_wc, *_ = brute(func=self.cn_corr, ranges=self.constraints, finish=fmin, full_output=True)
                self.corr_wc = corr_wc
                self.cn_wc = [r[int(i)] for (r,i) in zip(self.ranges, idxs)]
                if start == 1: # reintroduce -1 if it was there
                    self.cn_wc = [-1] + self.cn_wc
            self.cn_assignment_status = 1

        if plot:
            X = self.training_WC.loc[:,['tot_count', 'wf']].values
            
            fig, ax = plt.subplots(figsize=(6,3), ncols=2)
            lbls = self.GNB_wc.predict(X)
            start = 1 if (-1 in self.clusters) else 0
            lbls = np.take(self.cn_wc, lbls.astype(np.int8)+start) # +1 if labels start at -1
            sns.scatterplot(x=X[:,0], y=X[:,1],
                            hue=lbls, legend='full', palette=cc.glasbey_dark,
                            linewidth=0, marker='.', alpha=.5,
                            ax=ax[0])
            sns.move_legend(ax[0], 'upper left', bbox_to_anchor=(1,1))
            ax[1].set_axis_off()

    
    def get_ratios(self, nmin, nmax, cnmin, cnmax, plot=False):
    
        copy_numbers = range(cnmin, cnmax+1)
        nn = np.linspace(nmin, nmax, len(copy_numbers))
        
        refs = []
        cn_seq = []
          
        for n, cn in zip(nn, copy_numbers):
            for m in range(1,cn):
                refs.append([n, m/cn])
                cn_seq.append(cn)

        refs = np.array(refs)
        if plot:
            fig, ax = plt.subplots(figsize=(5,5))
            sns.scatterplot(x=refs[:,0], y=refs[:,1])
        return refs, cn_seq
               

    def cn_corr(self, params):
        cn_pred = []
        for r, p in zip(self.ranges, params):
            cn_pred.append(r[int(p)])
        if all([i == cn_pred[0] for i in cn_pred]):
            return 0
        
        cn_pred = np.array(cn_pred)
        sel = self.training_WC['cluster'] != -1
        x = self.training_WC.loc[sel,'tot_count']
        p = self.training_WC.loc[sel,'cluster'].astype(np.int8)
        w = self.training_WC.loc[sel,'weight']
        
        q = np.select([p == i for i in np.unique(p)], [cn_pred[j] for j in np.unique(p)])

        out = self.wcorr(x, q, w) * -1 # minimize
        return out
    
    def m(self, x, w):
        return np.sum(x * w) / np.sum(w)
    def cov(self, x, y, w):
        return np.sum(w * (x - self.m(x, w)) * (y - self.m(y, w))) / np.sum(w)
    def wcorr(self, x, y, w):
        return self.cov(x, y, w) / np.sqrt(self.cov(x, x, w) * self.cov(y, y, w))
    

    def fit_WW(self, cn1=None, calibrated=False, plot=True):
        # if cn1 is None, fitting copy number 1 is skipped
        # if cn1 is True, otsu threshold is estimated
        # if cn1 is a number, then it is considered the threshold 
        counts = self.training_WC.copy()
        counts.loc[:, 'wf'] = 0 # remove all watson fraction info

        # collect copy number one labels
        if cn1 is not None:
            cm = self.counts_merged.copy()
            cm = cm.loc[(~cm['wf_pass'])&(cm['low_pass'])&(cm['size_pass']), :]
            cm.loc[:, 'wf'] = 0
            if cn1 is True:
                # this wil allow re-training, while keeping cn_wc as is
                l = self.GNB_wc.theta_.shape[0]
                # this will account for edge cases where the minimum copy number above 1 is not 2
                cnmin = np.array(self.cn_wc)
                cnmin = cnmin[cnmin >1].min()
                mu = self.GNB_wc.theta_[:,0][np.array(self.cn_wc[:l]) == cnmin].mean()
                var = self.GNB_wc.var_[:,0][np.array(self.cn_wc[:l]) == cnmin].mean()
                t = (mu - ( np.sqrt(var) * 2.55 )) * self.norm_coeffs['tot_count']
            else:
                t = cn1
            print('threshold {:.2f}'.format(t))
            print(cm.shape)
            if plot:
                fig, ax = plt.subplots(figsize=(6,6), nrows=2, sharex=True)
                sns.histplot(cm['tot_count'], bins=np.linspace(0, cm['tot_count'].max(), 128), ax=ax[0])
                ax[0].axvline(t, linestyle='--', color='gray', alpha=.7)
                ax[0].set_ylabel('filtered WC count')
                cm2 = self.counts_merged.copy()
                cm2.loc[(cm2['wf_pass'])&(cm2['low_pass'])&(cm2['size_pass']), :]
                sns.histplot(cm2['tot_count'], bins=np.linspace(0, cm['tot_count'].max(), 128), ax=ax[1])
                ax[1].set_ylabel('filtered WW count')
                ax[1].axvline(t, linestyle='--', color='gray', alpha=.7)

            cm = cm.loc[cm['tot_count'] < t, :]
            print(cm.shape)
                        
            
            cm.loc[:, 'tot_count'] = cm['tot_count'] / self.norm_coeffs['tot_count']
            cm['cluster'] = counts['cluster'].max() + 1
            self.cn_wc.append(1)
            cm['weight'] = counts['weight'].mean()
            cm['reachability'] = np.nan
            cm = cm.loc[:,counts.columns]
            counts = pd.concat([counts, cm], axis=0)
            #counts['size'] = counts['end'] - counts['start']
            #counts['weight'] = (counts['size'] - counts['size'].min()) / (counts['size'].max() - counts['size'].min()) 
            
        # initialize the GMM
        if self.GNB_ww_status == 0:
            start = 1 if (-1 in self.clusters) else 0
            l = np.take(self.cn_wc, counts.loc[:, 'cluster'].astype(np.int8)+start)
            w = counts.loc[:, 'weight'].values
            
            self.GNB_ww = GaussianNB()
            self.GNB_ww.fit(X=counts.loc[:, ['tot_count', 'wf']].values, # only count info
                            y=l,
                            sample_weight=w)
            if calibrated:
                cal_GNB_ww = CalibratedClassifierCV(self.GNB_ww, cv=3, method='sigmoid', n_jobs=-1)
                cal_GNB_ww.fit(X=counts.loc[:, ['tot_count', 'wf']].values, # only count info
                               y=l,
                               sample_weight=w)
                self.GNB_ww = cal_GNB_ww

        if plot:
            self._GMM_ww_plot(counts.loc[:, ['tot_count', 'wf']].values)
        
        self.training_WW = counts

        self.GNB_ww_status = 1


    def _GMM_ww_plot(self, X):
        probs = self.GNB_ww.predict_proba(np.stack([np.linspace(0,1,500), np.zeros(500)]).T)

        fig, ax = plt.subplots(figsize=(3,3))
        sns.histplot(x=X[:,0], ax=ax)
        ax.set_ylabel('count')
        tax = ax.twinx()
        tax.set_ylim(0,1)
        tax.set_ylabel('probability')
        for i in range(probs.shape[1]):
            sns.lineplot(x=np.linspace(0,1,500), y=probs[:,i], color=cc.glasbey_dark[i], ax=tax)


    def predict_copy_number(self, aggregate_predictions=True, segm_id_col=None, plot=False):
        if segm_id_col is None:
            if self._supersegments:
                segm_id_col = 'super_segm_id'
            else:
                segm_id_col = 'segm_id_1'
        warnings.warn('predicting on {}'.format(segm_id_col))

        X = self.counts_merged.copy()
        
        X['tot_count'] = X['tot_count'] / self.norm_coeffs['tot_count']
        X.fillna(0, inplace=True)
        Xwc = X.loc[X['wf_pass'], :]
        Xww = X.loc[~X['wf_pass'], :]

        start = 1 if (-1 in self.clusters) else 0
        preds_cluster = self.GNB_wc.predict(Xwc.loc[:, ['tot_count', 'wf']].values)
        preds_wc = np.take(self.cn_wc, preds_cluster.astype(np.int8)+start) # +1 because labels start at -1
        probs_wc = self.GNB_wc.predict_proba(Xwc.loc[:, ['tot_count', 'wf']].values)
        unc_wc = np.sort(probs_wc, axis=1)[:,-2:]
        unc_wc = 1- (unc_wc[:,1] - unc_wc[:,0])
        
        preds_ww = self.GNB_ww.predict(Xww.loc[:, ['tot_count', 'wf']].values)
        preds_ww = preds_ww.astype(np.int8)
        probs_ww = self.GNB_ww.predict_proba(Xww.loc[:, ['tot_count', 'wf']].values)
        unc_ww = np.sort(probs_ww, axis=1)[:,-2:]
        unc_ww = 1- (unc_ww[:,1] - unc_ww[:,0])
        
        # aggregate predictions
        X['pred'] = np.nan
        X['prob'] = np.nan
        X['unc'] = np.nan
        X['classifier'] = np.nan
        X.loc[X['wf_pass'], 'pred'] = preds_wc
        X.loc[X['wf_pass'], 'pred_cluster'] = preds_cluster
        X.loc[X['wf_pass'], 'prob'] = np.max(probs_wc,axis=1)
        X.loc[X['wf_pass'], 'unc'] = unc_wc
        X.loc[X['wf_pass'], 'classifier'] = 'WC'
        X.loc[~X['wf_pass'], 'pred'] = preds_ww
        X.loc[~X['wf_pass'], 'prob'] = np.max(probs_ww,axis=1)
        X.loc[~X['wf_pass'], 'unc'] = unc_ww
        X.loc[~X['wf_pass'], 'classifier'] = 'WW'
        
        # UNCERTAIN REGIONS
        sel_unc = (X['pred'] == -1)
        if sel_unc.any():
            Xunc = X.loc[sel_unc, ['tot_count', 'wf']]
            Xunc['wf'] = 0.0
            preds_unc = self.GNB_ww.predict(Xunc.loc[:, ['tot_count', 'wf']].values)
            probs_unc = self.GNB_ww.predict_proba(Xunc.loc[:, ['tot_count', 'wf']].values)
            unc_unc = np.sort(probs_unc, axis=1)[:,-2:]
            unc_unc = 1- (unc_unc[:,1] - unc_unc[:,0])
            X.loc[sel_unc, 'pred'] = preds_unc
            X.loc[sel_unc, 'pred_cluster'] = np.nan
            X.loc[sel_unc, 'prob'] = np.max(probs_unc, axis=1)
            X.loc[sel_unc, 'unc'] = unc_unc
            X.loc[sel_unc, 'classifier'] = 'unc'

        # adjust high/low
        if isinstance(self.GNB_ww, CalibratedClassifierCV):
            count_means = self.GNB_ww.estimator.theta_[start:,0]
        else:    
            count_means = self.GNB_ww.theta_[start:,0] # exclude -1 component
        mean_min = np.nanmin(count_means)
        mean_max = np.nanmax(count_means)
        pred_high = int(X['pred'].max() + 1)
        X.loc[(X['pred'] == -1).values & (X['tot_count'] >= mean_max).values, 'pred'] = pred_high
        # X.loc[(X['pred'] == -1).values & (X['tot_count'] <= mean_min).values, 'pred'] = 1 # now taken care of in fit_WW
        X.loc[(X['wf_pass']&(X['pred'] == 1))|(~X['low_pass']), 'pred'] = -1 # correction for noise

        X['unc'] = X['unc'].astype(np.float64)
        X['prob'] = X['prob'].astype(np.float64)
        X['pred'] = X['pred'].astype(int)
        
        pred_cols = ['pred', 'pred_cluster', 'prob', 'unc', 'classifier']
        self.counts_merged.loc[:, pred_cols] = X.loc[:, pred_cols]
        
        self.counts = self.counts.loc[:, ~self.counts.columns.isin(pred_cols)] # reset previous output

        self.counts = pd.merge(self.counts, self.counts_merged[[segm_id_col] + pred_cols], on=segm_id_col, how='left')

        if plot:
            fig, ax = plt.subplots(figsize=(6,3), ncols=2)
            sns.scatterplot(data=self.counts_merged, x='tot_count', y='wf', 
                            hue='prob', palette=cc.cm.rainbow, legend=False, alpha=.5,
                            marker='.', linewidth=0, ax=ax[0])
            sns.scatterplot(data=self.counts_merged, x='tot_count', y='wf', 
                            hue='pred', legend='full', palette=cc.glasbey_dark, alpha=.5,
                            marker='.', linewidth=0, ax=ax[1])
            sns.move_legend(ax[1], 'upper left', bbox_to_anchor=(1,1))

        if aggregate_predictions:
            pass
            # # TODO multiply probabilities to get the combined one
            # # how about uncertainty?
            # lt = self.counts_merged.loc[~self.counts_merged['low_pass'], 'tot_count'].max()
            # self.counts['edge_0'] = False
            # self.counts.loc[self.counts.groupby(['cell', 'chrom']).head(1).index, 'edge_0'] = True
            # self.counts.loc[self.counts['class'] != self.counts['class'].shift(1), 'edge_0'] = True
            # self.counts.loc[self.counts['classifier'] != self.counts['classifier'].shift(1), 'edge_0'] = True
            # self.counts['edge_0'] = self.counts['edge_0'] | (self.counts['pred'].diff().ne(0) !=0)
            # self.counts['segm_id_1'] = self.counts['edge_0'].cumsum()
            # self.counts_merged = merge_bins(self.counts, bin_size=None, bin_idxs=self.counts['segm_id_1'], agg_mode='mean')
            # self.counts_merged.rename({'merged_bin': 'segm_id_1'}, axis='columns', inplace=True)
            # self.counts_merged['wf'] = (self.counts_merged['w'] / self.counts_merged['tot_count']).fillna(0)
            # self.filter_counts()
            # self.predict_copy_number(aggregate_predictions=False, plot=True)

        return (self.counts, self.counts_merged)
    

    def merge_supersegments(self, edge_column=None):
        '''adjacent segments with identical predicted copy number and wc ratio are joined together in super-segments'''
        counts = self.counts.copy()
        cm = self.counts_merged.copy()
        if edge_column is None:
            edges = (cm['changepoint_pred']|cm['changepoint_ratio'])
            edges[cm.groupby(['cell', 'chrom']).head(1).index] = True
            #edges[(cm['excluded'] != cm['exclude'].shift(1))] = True
        else:
            edges = cm[edge_column]

        cm['super_edges'] = edges
        #cm['super_segm_id'] = cm['super_edges'].cumsum()
        edge_coords = cm.loc[:, ['cell', 'chrom', 'start', 'super_edges']]
        
        counts = pd.merge(counts, edge_coords, on=['cell', 'chrom', 'start'], how='left')
        counts['super_edges'].fillna(False, inplace=True)
        counts['super_segm_id'] = counts['super_edges'].cumsum()

        counts_merged = merge_bins(counts, bin_size=None, bin_idxs=counts['super_segm_id'], agg_mode='stats')
        counts_merged.rename({'merged_bin': 'super_segm_id', 'tot_count_count': 'n_bins'}, axis='columns', inplace=True)
        counts_merged['wf'] = counts_merged['w'] / counts_merged['tot_count']

        self.counts = counts
        self.counts_merged = counts_merged
        self._supersegments = True


    def pred_qc_pass(self, **kwargs):
        qc_cols = [
            'prob_pass', 'exclusion_pass', 'low_count_bins', 'excluded_count_bins',
            'low_count_score', 'low_count_pass', 'is_WC', 'cn1_pass', 'noise_pass',
            'n_segm_pass', 'pred_QC_pass'
            ]
        if self.counts_merged.columns.isin(qc_cols).any():
            self.counts_merged = self.counts_merged.loc[:, ~self.counts_merged.columns.isin(qc_cols)]
            warnings.warn('discarding previously existing pred QC columns')
        qc = qc_pass(self, **kwargs)
        self.counts = qc.counts
        self.counts_merged = qc.counts_merged
        self._pred_qc = True

    
    def get_changepoints(self, **kwargs):
        cp_cols = [
            'changepoint_pred',
            'changepoint_ratio',
            'changepoint_SCE'
        ]
        if self.counts_merged.columns.isin(cp_cols).any():
            self.counts_merged = self.counts_merged.loc[:, ~self.counts_merged.columns.isin(cp_cols)]
            warnings.warn('discarding previously existing pred QC columns')
        
        cp = get_changepoints(self, **kwargs)
        self.counts = cp.counts
        self.counts_merged = cp.counts_merged
        self._changepoints = True
