import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
from .plot import reorder_chroms


class SVAnnotation:
    # copy, size, haplotype
    def __init__(self, counts, figsize_factor=None):
        
        self.modes = ['copy', 'size', 'haplotype']
        self.figsize_factor =  1 if figsize_factor is None else figsize_factor
        
        # create empty table from count file or import previous annotation
        if isinstance(counts, pd.DataFrame):
            self.table = self.get_table(counts)
        elif isinstance(counts, dict):
            if all([k in counts.keys() for k in self.modes]):
                self.table = counts
            else:
                raise ValueError('annotation dictionary does not contain all modes: {}'.format(self.modes))
        
        self._legend = pd.DataFrame(
            [
                ['copy', 'gain', 'G', 'limegreen', 1],
                ['copy', 'loss', 'L', 'tomato', 2],
                ['copy', 'complex', 'C', 'skyblue', 3],
                ['copy', 'chromothripsis', 'CT', 'mediumorchid', 4],
                ['copy', 'none', 'N', 'gainsboro', 0],
                ['size', 'terminal', 'T', 'limegreen', 1],
                ['size', 'whole', 'W', 'tomato', 2],
                ['size', 'arm', 'A', 'skyblue', 3],
                ['size', 'interstitial', 'I', 'mediumorchid', 4],
                ['size', 'none', 'N', 'gainsboro', 0],
                ['haplotype', 'H1', '1', 'tomato', 1],
                ['haplotype', 'H2', '2', 'skyblue', 2],
                ['haplotype', 'none', 'N', 'gainsboro', 0],
             ],
            columns = ['mode', 'type', 'abbr', 'color', 'int']
        )
    
    def get_table(self, counts):
        chroms = counts['chrom'].unique()
        chroms = reorder_chroms(chroms)
        cells = counts['cell'].unique()
        table = pd.DataFrame(columns=cells, index=chroms)
        table.fillna('N', inplace=True)
        return {'copy': table, 'size': table.copy(), 'haplotype': table.copy()}
    
    def add(self, cell, chrom, copy=None, size=None, haplotype=None):
        if self._legend[self._legend['mode']=='copy']['abbr'].isin([copy]).any(): # gain, loss, complex, chromothripsis
            self.table['copy'].loc[chrom, cell] = copy
        elif copy is None:
            self.table['copy'].loc[chrom, cell] = 'N'
        else:
            raise ValueError('invalid copy annotation: {}'.format(copy))
            
        if self._legend[self._legend['mode']=='size']['abbr'].isin([size]).any(): # whole chromosome, arm, terminal, interstitial
            self.table['size'].loc[chrom, cell] = size
        elif size is None:
            self.table['size'].loc[chrom, cell] = 'N'
        else:
            raise ValueError('invalid size annotation: {}'.format(size))
            
        if self._legend[self._legend['mode']=='haplotype']['abbr'].isin([haplotype]).any(): # whole chromosome, arm, terminal, interstitial
            self.table['haplotype'].loc[chrom, cell] = haplotype
        elif haplotype is None:
            self.table['haplotype'].loc[chrom, cell] = 'N'
        else:
            raise ValueError('invalid haplotype annotation: {}'.format(haplotype))
    
    def value_to_int(self, v):
        return self._legend.loc[self._legend['abbr'] == v, 'int'].iloc[0]
        
    def sort_table(self, by):
        if by not in self.modes:
            raise ValueError('Can sort only by copy, size and haplotype')
        
        # sorting table ...
        sorted_table = self.table.copy()
        for k in self.modes:
            sorted_table[k].fillna('N', inplace=True)
            sorted_table[k] = sorted_table[k].applymap(self.value_to_int)
        
        # melt table
        melt = pd.melt(sorted_table[by].reset_index(), id_vars='index', value_name='abn', var_name='cell')
        # reverse order of chromosome to match abn_bool
        melt['index'] = pd.Categorical(melt['index'], categories=reorder_chroms(melt['index'].unique())[::-1])
        melt['abn_bool'] = melt['abn'] > 0
        # reorder and take first abnormal chromosome
        melt_sorted = melt.sort_values(by=['abn_bool', 'index'], ascending=False).groupby('cell').agg('first')
        # order now by first abnormal chromosome
        melt_sorted = melt_sorted.sort_values(by=['abn_bool', 'index'], ascending=False)
        # sort tables
        for t in self.modes:
            sorted_table[t] = sorted_table[t].loc[reorder_chroms(sorted_table[t].index), melt_sorted.index.to_list()]
        return sorted_table
    
    def display(self, sort=True):
        df = self.sort_table(by='copy')
        if sort==False:
            df = self.table.copy()
            for k in self.modes:
                df[k].fillna('N', inplace=True)
                df[k] = df[k].applymap(self.value_to_int)
        
        fig, ax = plt.subplots(figsize=(int(.25*len(df['copy'].columns))*self.figsize_factor,15*self.figsize_factor), nrows=3, ncols=1, sharex=True)
        
        for i, (mode, table) in enumerate(df.items()):

            l = self._legend[self._legend['mode'] == mode]
            n = l.shape[0]
            
            # cmap
            cmap = sns.color_palette(l.sort_values(by='int')['color'].values, n)
            
            # heatmap
            sns.heatmap(
                table, 
                cmap=cmap, 
                square=True,
                linewidth=0.3, cbar_kws={"shrink": .5},
                xticklabels=True, yticklabels=True,
                vmax=n, vmin=0,                
                ax=ax[i]
            ) 
            
            # modify colorbar:
            colorbar = ax[i].collections[0].colorbar 
            colorbar.set_ticks([colorbar.vmin + (0.5 + i) for i in range(n)])            
            colorbar.set_ticklabels([l.loc[l['int'] == j, 'type'].iloc[0] for j in range(n)])

        return fig
            
    def concat(self, SV_annotation):
        t = SV_annotation.table
        for k in self.modes:
            self.table[k] = pd.concat([self.table[k], t[k]], axis=1)
            
    def summary_plots(self, kind='count', figsize=None, ylims=None, rotate_labels=None):
        
        summaries = []
        figs = []
        for n, mode in enumerate(self.modes):
            self.table[mode].fillna('N', inplace=True)
            n_cells = len(self.table[mode].columns)
            melt = pd.melt(self.table[mode].T, var_name='chrom', value_name=mode)
            l = self._legend[self._legend['mode'] == mode]
            
            # substitute abbreviation with long form
            melt[mode] = melt[mode].apply(lambda x: l.loc[(self._legend['abbr']==x), 'type'].iloc[0])
            
            # generate summary
            summary = melt[~(melt[mode] == 'none')][mode].value_counts().reset_index()
            
            if not summary.empty:
                cats = self._legend.loc[(self._legend['mode'] == mode),'type']
                for cat in cats:
                    if (not cat in summary[mode].values) and (cat != 'none'):
                        summary = pd.concat([summary, pd.DataFrame([[cat, 0]], columns=[mode, 'count'])], axis=0)
                
                summaries.append({mode: summary})

                if kind == 'frequency':
                    summary.rename({'count': 'frequency'}, axis='columns', inplace=True)
                    summary['frequency'] = summary['frequency']/n_cells
                elif kind == 'percentage':
                    summary.rename({'count': 'percentage'}, axis='columns', inplace=True)
                    summary['percentage'] = summary['percentage']/n_cells*100

                if figsize is None:
                    figsize = (8,5)
                fig, ax = plt.subplots(figsize=figsize)
                pal = pd.Series(l['color'].values, index=l['type']).to_dict()
                sns.barplot(data=summary, x=mode, y=kind, 
                            palette=pal, ax=ax)
                ax.set_xlabel('abnormality {}'.format(mode))
                if ylims is not None:
                    ax.set_ylim(0, ylims[n])
                if rotate_labels is not None:
                    fig.autofmt_xdate(rotation=rotate_labels)
                figs.append(fig)
        return summaries, figs
    
    def summary_heatmap(self, modes=('copy', 'size'), vmax=None, normalize=False, figsize=None):
        modes = ('copy', 'size')
        melts = []
        for mode in modes:
            melt = pd.melt(self.table[mode].reset_index(),id_vars='index', var_name='cell', value_name=mode)
            melt[mode] = pd.Categorical(melt[mode], categories=self._legend.loc[self._legend['mode'] == mode, 'abbr'].to_list())
            melt.rename({'index': 'chrom'}, axis='columns', inplace=True)
            melts.append(melt)
        
        merge = pd.merge(*melts, on=['chrom', 'cell'], how='left')
        rawmatrix = pd.crosstab(merge[modes[0]], merge[modes[1]], dropna=False, normalize=normalize)
        
        merge = merge[(merge[modes[0]] != 'N') & (merge[modes[1]] != 'N')]
        matrix = pd.crosstab(merge[modes[0]], merge[modes[1]], dropna=False, normalize=normalize)
        
        
        for n, mode in enumerate(modes):
            order = self._legend.loc[self._legend['mode'] == mode, 'abbr'].to_list()
            order = [o for o in order if o != 'N']
            if n == 0:
                matrix = matrix.loc[order, :]
            elif n == 1:
                matrix = matrix.loc[:, order]
        if figsize is None:
            figsize = (6,6)
        fig, ax = plt.subplots(figsize=figsize)
        sns.heatmap(matrix, cmap='viridis', annot=True, vmax=vmax, ax=ax)
        return rawmatrix, fig