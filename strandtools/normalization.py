import numpy as np
import pandas as pd
from tqdm.notebook import tqdm
from scipy.linalg import null_space
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.api as sm



def merge_bins(counts, bin_size, bin_idxs=None, agg_mode='mean'):
    counts.loc[:,'merged_bin'] = counts['start'] // bin_size if bin_idxs is None else bin_idxs
    G = counts.groupby(['cell', 'chrom', 'merged_bin'])

    df = []
    df.append(G['start'].first())
    df.append(G['end'].last())
    df.append(G['class'].agg(pd.Series.mode))
    if agg_mode == 'sum':
        df.append(G['w'].sum())
        df.append(G['c'].sum())
        df.append(G['tot_count'].sum())
    elif agg_mode == 'mean':
        df.append(G['w'].mean())
        df.append(G['c'].mean())
        df.append(G['tot_count'].mean())
    elif agg_mode == 'stats':
        df.append(G['w'].mean())
        df.append(G['c'].mean())
        df.append(G['tot_count'].mean())
        df.append(pd.Series(G['tot_count'].std(), name='tot_count_std'))
        df.append(pd.Series(G['tot_count'].count(), name='tot_count_count'))
    df = pd.concat(df, axis=1).reset_index()
    #df.drop('merged_bin', axis='columns', inplace=True)
    
    return df

def aggregate_bins(counts, bin_size):
    counts_bin_size = counts['start'].diff(1).mode().iloc[0]
    if (bin_size % counts_bin_size) > 0:
        raise ValueError('bin_size must be a multiple of {}'.format(counts_bin_size))
    n_bins = int(bin_size / counts_bin_size)
    G = counts.groupby(['cell', 'chrom'])

    # rolling mean over chromosome length
    # mean at start and end of chromosome is calculated with a smaller bin window (min_periods=1)
    # window is centered on the target bin (center=True)
    c_w = G[['start', 'w']].rolling(n_bins, center=True, min_periods=1, on='start').mean().reset_index().sort_values(by=['cell', 'chrom','start'])
    c_c = G[['start', 'c']].rolling(n_bins, center=True, min_periods=1, on='start').mean().reset_index().sort_values(by=['cell', 'chrom','start'])
    c_w.drop(['level_2'], axis=1, inplace=True)
    c_c.drop(['level_2'], axis=1, inplace=True)
    df = pd.merge(counts, c_w, on=['cell', 'chrom', 'start'], how='left', suffixes=['_raw', ''])
    df = pd.merge(df, c_c, on=['cell', 'chrom', 'start'], how='left', suffixes=['_raw', ''])
    df['tot_count'] = df['w'] + df['c']
    df = df.loc[:, df.columns.isin(['sample', 'cell', 'chrom', 'start', 'end', 'class', 'w', 'c', 'tot_count'])]
    return df


def anscombe_vst(x, phi):
    return np.arcsinh(np.sqrt((x+0.375)/(1/phi-0.75)))

def to_matrix(counts, count_col='tot_count'):
    mat = pd.pivot_table(data=counts, index=['chrom', 'start', 'end'], columns='cell', values=count_col)
    return mat

def calc_residual_variance(mat):
    # this function assumes that all cells belong to same sample! no experimental design
    res = np.matmul(mat, null_space(np.ones((mat.shape[1],mat.shape[1]))))
    res = np.sqrt(np.mean(np.power(res,2), axis=1))
    res = res.std()/res.mean()
    return res

def dispersion_optimization(counts, phi_min=0, phi_max=.5, phi_steps=50, plot=False,
                            count_col='tot_count'):
    residuals = []
    for phi in tqdm(np.linspace(phi_min, phi_max, phi_steps)):
        counts['{}_vst'.format(count_col)] = anscombe_vst(counts[count_col], phi)
        mat = to_matrix(counts, '{}_vst'.format(count_col))
        res = calc_residual_variance(mat)
        residuals.append({'phi': phi, 'res': res})
    residuals = pd.DataFrame(residuals)
    
    opt_phi = residuals.loc[residuals['res'].argmin(), 'phi']
    
    residuals['mark'] = (residuals['phi'] == opt_phi)
    if plot:
        fig, ax = plt.subplots(figsize=(4,4))
        sns.scatterplot(data = residuals, 
                        x='phi', y='res', hue='mark', 
                        legend=False, ax=ax)
        ax.set_xlabel('dispersion')
        ax.set_ylabel('cv(residual standard deviation)')
    return opt_phi

def meanvar_plot(counts, count_col='tot_count', vst_col='tot_count_vst'):
    df = counts.groupby(['chrom','start','end'])[count_col].agg(['mean', 'std', 'var']).reset_index()
    fig, ax = plt.subplots(figsize=(10,10), ncols=2, nrows=2)

    sns.scatterplot(data=df, x='mean', y='var',
                    alpha=.3, marker='.', linewidth=0,
                    ax=ax[0,0])
    ax[0,0].set_yscale('log')
    ax[0,0].set_xscale('log')
    ax[0,0].set_xlabel('mean bin read count')
    ax[0,0].set_ylabel('variance bin read count')
    ax[0,0].set_title('before')
    
    df = counts.groupby(['chrom','start','end'])[vst_col].agg(['mean', 'std', 'var']).reset_index()
    
    sns.scatterplot(data=df, x='mean', y='var',
                    alpha=.3, marker='.', linewidth=0,
                    ax=ax[0,1])
    ax[0,1].set_yscale('log')
    ax[0,1].set_xscale('log')
    ax[0,1].set_xlabel('mean bin read count')
    ax[0,1].set_ylabel('variance bin read count')
    ax[0,1].set_title('after')
    
    counts['tot_count'].hist(bins=100, ax=ax[1,0])
    counts['tot_count_vst'].hist(bins=100, ax=ax[1,1])

def scale_counts(counts, count_col='tot_count', vst_col='tot_count_vst'):
    k = counts['tot_count'].mean() / counts['tot_count_vst'].mean()
    scaled_counts = counts['tot_count_vst']*k
    counts['w'] = (counts['w'] / counts['tot_count'] * scaled_counts).fillna(0)
    counts['c'] = (counts['c'] / counts['tot_count'] * scaled_counts).fillna(0)
    counts['tot_count'] = scaled_counts
    return counts

def library_normalization(counts, min_reads=100000, plot=False):
    tot_reads = counts.groupby('cell')['tot_count'].sum().sort_values()
    sel_cells = tot_reads[(tot_reads >= min_reads)].index
    counts_filt = counts.loc[counts['cell'].isin(sel_cells), :]
    
    c_t0 = counts_filt.groupby('cell')['tot_count'].sum()
    
    mat = pd.pivot_table(data=counts_filt, index=['chrom', 'start', 'end'], columns='cell', values='tot_count')
    
    # exclude most unstable bins
    geom_means = np.mean(np.log(mat+1), axis=1)
    deciles = pd.qcut(geom_means, q=10, labels=False)
    bin_sel = deciles.between(left=3, right=7)
    mat = mat.loc[bin_sel, :]
    
    matlog = np.log(mat)
    mean_log_count = matlog.mean(axis=1)
    matlog = matlog[~mean_log_count.isin([-np.inf, np.inf])]
    matlog_norm = matlog.apply(lambda x: x - matlog.mean(axis=1), axis=0)
    matlog_norm = matlog_norm.replace(np.inf, np.nan)
    matlog_norm = matlog_norm[~matlog_norm.isna().any(axis=1)]
    scaling_factors = 1/np.exp(matlog_norm.median(axis=0))
    scaling_factors.name = 'f'
    
    counts_norm = pd.merge(counts_filt, scaling_factors.reset_index(), on='cell', how='left')
    counts_norm['tot_count'] = counts_norm['tot_count'] * counts_norm['f']
    counts_norm['w'] = counts_norm['w'] * counts_norm['f']
    counts_norm['c'] = counts_norm['c'] * counts_norm['f']
    
    c_t1 = counts_norm.groupby('cell')['tot_count'].sum()
    c_comparison = pd.concat([c_t0, c_t1], axis=1)
    c_comparison.columns = ['before', 'after']
    c_comparison = pd.melt(c_comparison.reset_index(), id_vars='cell', value_name='tot_count', var_name='normalization')
    
    if plot:
        fig, ax = plt.subplots(figsize=(8,4), ncols=2)
        sns.stripplot(data=scaling_factors, ax=ax[0])
        ax[0].set_xlabel('scaling factors')
        ax[0].axhline(1, linestyle='--', color='lightgray')
        sns.scatterplot(data=c_comparison, x='normalization', y='tot_count', hue='cell', legend=False, ax=ax[1])
        sns.lineplot(data=c_comparison, x='normalization', y='tot_count', hue='cell', alpha=.3, legend=False, ax=ax[1])
        ax[1].set_xlabel('library size normalization')
        ax[1].set_ylabel('total read count')
        fig.tight_layout()
    
    return counts_norm

def vst(counts, phi=None, phi_min=0.001, phi_max=1, phi_steps=50, plot=True):
    counts = counts.copy()
    
    if phi is None:
        phi = dispersion_optimization(counts, phi_min=phi_min, phi_max=phi_max, phi_steps=phi_steps, plot=plot)
    
    counts['tot_count_vst'] = anscombe_vst(counts['tot_count'], phi)
    shift = anscombe_vst(0, phi)
    counts['tot_count_vst'] = counts['tot_count_vst'] - shift
    
    if plot:
        meanvar_plot(counts)
    
    counts_vst = scale_counts(counts)

    return counts_vst.loc[:,['chrom', 'start', 'end', 'sample', 'cell', 'w', 'c', 'tot_count', 'class']]


def gc_correction(counts, min_count=5, n_subsample=100, plot=False, GC_path=None, **kwargs):
    # load GC data
    if GC_path is None:
        GC = pd.read_csv("../resources/GC_matrix_200000.txt", sep='\t')
    else:
        GC = pd.read_csv(GC_path, sep='\t')
    GC.rename({'GC%': 'GC'}, axis='columns', inplace=True)
    GC = GC.replace('none', np.nan)

    counts = pd.merge(counts, GC[['chrom', 'start', 'GC']], on=['chrom', 'start'], how='left')
    counts['GC'] = pd.to_numeric(counts['GC'])
    
    # lowess fit
    lowess = sm.nonparametric.lowess
    c = counts[(counts['tot_count'] >= min_count)]
    c['GC'] = pd.to_numeric(c['GC'])
    c['log_count_norm'] = np.log(c['tot_count']/c['tot_count'].median()) 
    
    s = c[~c['GC'].isna()]
    s['GC_bin'] = pd.qcut(s['GC'], q=10, labels=False)
    s = s.groupby('GC_bin', group_keys=False).apply(lambda x: x.sample(min(len(x), n_subsample)))
    
    z = lowess(s['log_count_norm'].astype(float).values, s['GC'].astype(float).values, **kwargs)

    # correction
    counts['tot_count_pred'] = counts['GC'].apply(lambda x: np.argmin(np.abs(x - z[:,0])))
    counts['tot_count_pred'].clip(upper=z.shape[0]-1, inplace=True)
    counts['tot_count_pred'] = counts['tot_count_pred'].apply(lambda x: z[x,1])
    counts['tot_count_gc'] = np.log(counts['tot_count'] / counts['tot_count'].median()) - counts['tot_count_pred']
    counts['tot_count_gc'] = np.exp(counts['tot_count_gc']) * counts['tot_count'].median()
    
    if plot:
        fig, ax = plt.subplots(figsize=(12,4), ncols=2)
        sns.scatterplot(data=s, x='GC', y='log_count_norm',
                        marker='.', linewidth=0, ax=ax[0])
        sns.lineplot(x=z[:,0], y=z[:,1], color='red', ax=ax[0])
        ax[0].set_xlabel('GC content')

        sns.scatterplot(data=counts.loc[s.index,:], x='GC', y='tot_count_gc',
                        marker='.', linewidth=0, ax=ax[1])
        ax[1].set_xlabel('GC content')
        fig.tight_layout()

    # propagate correction to w and c data
    # counts['w_f'] = counts['w'] / counts['tot_count']
    # counts['w'] = (counts['w_f'] * counts['tot_count_gc']).fillna(0)
    # counts['c'] = (counts['tot_count_gc'] - counts['w']).fillna(0)
    # counts['tot_count'] = counts['tot_count_gc'].fillna(0)
    
    counts['w'] = (counts['w'] / counts['tot_count'] * counts['tot_count_gc']).fillna(0)
    counts['c'] = (counts['c'] / counts['tot_count'] * counts['tot_count_gc']).fillna(0)
    counts['tot_count'] = counts['tot_count_gc'].fillna(0)
    
    return counts.loc[:,['chrom', 'start', 'end', 'sample', 'cell', 'w', 'c', 'tot_count', 'class']]


def smoothing(counts, window_size=20, apply=False, clip_negative=False):
    '''Filter out coordinated noise in the Strandseq data.'''

    cols = counts.columns
    trend = counts.groupby(['chrom', 'start'])['tot_count'].mean().reset_index()

    # rolling mean to smoothen signal and get like a local average
    trend['rolling_mean'] = trend['tot_count'].rolling(window=window_size, center=True, step=1, min_periods=1).median()
    # the difference between local average and pseudo-ref is the correction factor for that bin
    trend['tot_count_residual'] = trend['tot_count'] - trend['rolling_mean']

    counts = pd.merge(counts, trend[['chrom', 'start', 'tot_count_residual']], on=['chrom', 'start'], how='left')
    counts['tot_count_corr'] = (counts['tot_count'] - counts['tot_count_residual']).fillna(counts['tot_count'])

    # w : tot_count = w_corr : tot_count_corr
    counts['w_corr'] = (counts['w'] * counts['tot_count_corr']/counts['tot_count']).fillna(0)
    counts['c_corr'] = (counts['c'] * counts['tot_count_corr']/counts['tot_count']).fillna(0)

    if clip_negative:
        counts['tot_count_corr'] = counts['tot_count_corr'].clip(lower=0)
        counts['w_corr'] = counts['w_corr'].clip(lower=0)
        counts['c_corr'] = counts['c_corr'].clip(lower=0)

    if apply:
        counts['w'] = counts['w_corr'].copy()
        counts['c'] = counts['c_corr'].copy()
        counts['tot_count'] = counts['w'] + counts['c']
        counts = counts.loc[:, cols]

    return counts