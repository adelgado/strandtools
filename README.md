# strandtools

Strandtools is a collection of tools for copy-number calling, visualization and annotation from strand-seq data. It is specifically tailored for the detection of copy-number changes in single-cells, even in complex ploidy background, leveaging read-coverage and template ratio at the sample as well as single-cell level.

Main features:
- Library size and composition normalization
- GC content correction
- Variance stabilization with Anscombe transform
- Copy number prediciton with Naive Bayes classifiers
- Annotation tools
- Visualization tools for Strand-seq data

## Installation

Clone the repository

`git clone https://git.embl.de/cosenza/strandtools/ & cd strandtools`

Create a virtual environment (e.g. via conda or venv), strandtools was tested with Python 3.11.5 on Ubuntu 22.04.4 LTS. 

Environment creation example with conda:
`conda create -n strandtools_env python=3.11.5 jupyterlab ipywidgets && conda activate strandtools_env`

Then install strandtools

`pip install .`

If you want to use it in jupyter notebooks, do not forget to install the kernelspec

`python3 -m ipykernel install --user --name strandtools_env`

Start jupyter lab from the repository folder:

`jupyter lab`

## Getting started

To get familiar with strandtools, you can find a series of jupyter notebooks, with tutorials taking you through the main features of the package.  
For testing purposes, you can download some example data from the [MosaiCatcher](https://github.com/friendsofstrandseq/mosaicatcher-pipeline/) repository.

`wget https://github.com/friendsofstrandseq/mosaicatcher-testdata/raw/main/data_ALL_CHROM/RPE-BM510/counts/BM510x3.txt.raw.gz resources/`

Tutorials can be found in the **notebooks** folder:

The tutorials are meant to be run from the repository folder, so that the data can be found in the resources folder.

- [**strandtools_tutorial_multi-step_normalization.ipynb**](https://git.embl.de/cosenza/strandtools/-/blob/main/notebooks/strandtools_tutorial_multi-step_normalization.ipynb)  
this tutorial takes you through the first steps for library normalization, GC content correction and variance stabilization

- [**strandtools_tutorial_copy-number-calling.ipynb**](https://git.embl.de/cosenza/strandtools/-/blob/main/notebooks/strandtools_tutorial_copy-number-calling.ipynb)  
this tutorial explains how copy-number calling works and how parameters can be tweaked to improve calling on other datasets

- [**strandtools_tutorial_SV_annotation.ipynb**](https://git.embl.de/cosenza/strandtools/-/blob/main/notebooks/strandtools_tutorial_SV_annotation.ipynb)  
this tutorial shows one possible way of annotating data and get an overview of all copy number changes

- [**strandtools_tutorial_plots_01.ipynb**](https://git.embl.de/cosenza/strandtools/-/blob/main/notebooks/strandtools_tutorial_plots_01.ipynb)  
this notebook contain instructions on how to use basic Strand-seq plotting functions and integrate them in your own figures

- [**strandtools_tutorial_plots_02.ipynb**](https://git.embl.de/cosenza/strandtools/-/blob/main/notebooks/strandtools_tutorial_plots_02.ipynb)  
this notebook showcasees more advanced plots to visualize copy number changes and predictions

- [**strandtools_tutorial_code-only.ipynb**](https://git.embl.de/cosenza/strandtools/-/blob/main/notebooks/strandtools_tutorial_code-only.ipynb)  
this notebook contains a condensed version of the normalization and copy-number calling notebooks, taking you through the whole process, from raw data to final predictions  


## References

> MosaiCatcher v2 publication: Weber Thomas, Marco Raffaele Cosenza, and Jan Korbel. 2023. ‘MosaiCatcher v2: A Single-Cell Structural Variations Detection and Analysis Reference Framework Based on Strand-Seq’. Bioinformatics 39 (11): btad633. https://doi.org/10.1093/bioinformatics/btad633

> Strand-seq publication: Falconer, E., Hills, M., Naumann, U. et al. DNA template strand sequencing of single-cells maps genomic rearrangements at high resolution. Nat Methods 9, 1107–1112 (2012). https://doi.org/10.1038/nmeth.2206

> scTRIP/MosaiCatcher original publication: Sanders, A.D., Meiers, S., Ghareghani, M. et al. Single-cell analysis of structural variations and complex rearrangements with tri-channel processing. Nat Biotechnol 38, 343–354 (2020). https://doi.org/10.1038/s41587-019-0366-x


## Contributors

- Marco Raffaele Cosenza (maintainer and developer)

## Contact

Please, contact Marco Raffaele Cosenza (marco.cosenza(at)embl.de) in case you have any questions.

